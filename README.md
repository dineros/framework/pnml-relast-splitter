# Petri Net Splitter Tool

This is the 3rd tool of the DiNeROS model driven tool chain. The input of this tool as an (extended) PNML-described Global ROS Petri Net.
Based on the localization information contained in the corresponding subnets for target ROS node is created, theses subnets are containing special transitions and places respresenting the inputs and outputs of ROS publishers/subscribers or and service clients and servers.

## The Programflow

1. Global ROS Petri Net (**PNML**)
2. Parsed Representation (**Jastadd**)
3. Splitted Local ROS Petri Net (**Jastadd**)
4. Splitted Local ROS Petri Net (**PNML**)

## How Publisher and subscribers are transformed

1. Get location/subnet of source/target - places
2. Create new source and target Transition
3. Attributized infos need to be written back to XML
4. Reconnect Input Signals
5. Get all incoming arcs and connect them to new output transition
6. Gel all outgoing arcs and connect them to new input transition
7. Add new transitions to net
8. Remove "old transition"

## How Service Clients and Servers are tranformed

TBD

## Currently not supported pnml features

* reference-places and reference transitions