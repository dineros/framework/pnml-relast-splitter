package de.tudresden.inf.st.pnml.splitter.data;

import java.util.HashMap;

public class ClauseValuesDefinition {

    private HashMap<String, Boolean> defs;

    public ClauseValuesDefinition(){
        defs = new HashMap<String, Boolean>();
    }

    public HashMap<String, Boolean> getDefs() {
        return defs;
    }

    public Boolean getDef(String id) {
        return defs.get(id);
    }

    public void addDef(String id, Boolean def) {
        defs.put(id, def);
    }
}
