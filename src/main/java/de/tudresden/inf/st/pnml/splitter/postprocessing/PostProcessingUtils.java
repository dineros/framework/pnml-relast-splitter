package de.tudresden.inf.st.pnml.splitter.postprocessing;

import de.tudresden.inf.st.pnml.base.constants.PnmlConstants;
import de.tudresden.inf.st.pnml.splitter.copy.CopyPrimitiveElements;
import de.tudresden.inf.st.pnml.jastadd.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PostProcessingUtils {

    private static final Logger logger = LoggerFactory.getLogger(PostProcessingUtils.class);

    ///////////////////////////////
    // NET INFORMATION ////////////
    ///////////////////////////////

    private static Set<Transition> getTransitionsBySubnet(String subNet, PetriNet petriNet) {

        return petriNet.allTransitions().stream().filter(t -> t.asDinerosTransition()
                .getStaticTransitionInformation().getSubNet().equals(subNet)).collect(Collectors.toSet());
    }

    private static Set<String> getTransitionIDsBySubnet(String subNet, PetriNet petriNet) {

        Set<String> transitionIds = new HashSet<>();

        for (Transition t : getTransitionsBySubnet(subNet, petriNet)) {
            transitionIds.add(t.getId());
        }

        return transitionIds;
    }

    private static Set<String> getPlaceIDsBySubnet(String subNet, PetriNet petriNet) {

        Set<String> placeIds = new HashSet<>();

        for (Place p : getPlacesBySubnet(subNet, petriNet)) {
            placeIds.add(p.getId());
        }

        return placeIds;
    }

    private static Set<Place> getPlacesBySubnet(String subNet, PetriNet petriNet) {

        return petriNet.allPlaces().stream().filter(t -> t.asDinerosPlace()
                .getStaticPlaceInformation().getSubNet().equals(subNet)).collect(Collectors.toSet());
    }

    private static boolean pageIsEmpty(Page page) {
        for (PnObject o : page.getObjects()) {
            if (o.isPlaceObject() || o.isTransitionObject()
                    || o.isRefTransitionObject() || o.isRefPlaceObject()) {
                return false;
            } else if (o.isPageNode()) {
                if (!pageIsEmpty(o.asPage())) {
                    return false;
                }
            }
        }
        return true;
    }

    ///////////////////////////////
    // NET MANIPULATION ///////////
    ///////////////////////////////

    public static void cleanEmptyPages(PetriNet petriNet) {

        List<Page> pagesToRemove = new ArrayList<>();

        for (Page p : petriNet.allPages()) {
            if(pageIsEmpty(p)){
                System.out.println("Removing page: " + p.getId());
                pagesToRemove.add(p);
            }
        }

        pagesToRemove.forEach(ASTNode::removeSelf);
    }


    ///////////////////////////////
    // LOGGING ////////////////////
    ///////////////////////////////

    public static void printNet(PetriNet petriNet, boolean withArcs, boolean withToolSpecifics) {

        System.out.println("--------------- STRUCTURE ---------------");
        System.out.println("----------------- PLACES ----------------");

        for (Place p : petriNet.allPlaces()) {
            System.out.println("Place " + p.asDinerosPlace().getId() + " -- " + p.asDinerosPlace().getStaticPlaceInformation().getSubNet());
            if (p.getInitialMarking() != null) {
                System.out.println("--- Marking: " + p.getInitialMarking().getText());
            } else {
                System.out.println("--- Marking: NULL");
            }
        }

        System.out.println("-------------- TRANSITIONS --------------");

        for (Transition t : petriNet.allTransitions()) {
            System.out.println("Transition " + t.getId());
        }

        System.out.println("-------------- TRANSITION DETAILS --------------");

        for (Transition t : petriNet.allTransitions()) {

            if (t.asDinerosTransition().getStaticTransitionInformation().isServiceTransitionInformation()) {
                System.out.println("--- Transition: " + t.getId() + " subnet: " + t.asDinerosTransition().getStaticTransitionInformation().getSubNet()
                        + " service: " + t.asDinerosTransition().getStaticTransitionInformation().asServiceTransitionInformation().getServiceName() + " ---------");
            } else if (t.asDinerosTransition().getStaticTransitionInformation().isTopicTransitionInformation()) {
                System.out.println("--- Transition: " + t.getId() + " subnet: " + t.asDinerosTransition().getStaticTransitionInformation().getSubNet()
                        + " topic: " + t.asDinerosTransition().getStaticTransitionInformation().asTopicTransitionInformation().getTopic() + " ---------");
            } else {
                System.out.println("--- Transition: " + t.getId() + " subnet: " + t.asDinerosTransition().getStaticTransitionInformation().getSubNet() + " --- name: " + t.getName().getText());
            }

            for (Place p : t.asDinerosTransition().incomingPlaces()) {

                System.out.println("------ Inputplace:  " + p.getId() + " subnet: " + p.asDinerosPlace().getStaticPlaceInformation().getSubNet() + " ---------");
            }

            for (Place p : t.asDinerosTransition().outgoingPlaces()) {

                System.out.println("------ Outputplace: " + p.getId() + " subnet: " + p.asDinerosPlace().getStaticPlaceInformation().getSubNet() + " ---------");
            }
        }

        System.out.println("----------------- REF PLACES -----------------");

        for (RefPlace rp : petriNet.allRefPlaces()) {
            System.out.println("--- RefPlace: " + rp.getId());
        }

        System.out.println("----------------- REF TRANSITIONS -----------------");

        for (RefTransition rt : petriNet.allRefTransitions()) {
            System.out.println("--- RefTransition: " + rt.getId());
        }

        if (withArcs) {
            System.out.println("----------------- ARCS -----------------");

            for (Arc a : petriNet.allArcs()) {
                System.out.println("Arc: " + a.getId() + " -- source: " + a.getSource().getId() + " -- target: " + a.getTarget().getId());
            }
        }


        System.out.println("--------------- T SIGNALS (STATIC)---------------");

        for (Transition t : petriNet.allTransitions()) {
            DinerosTransition ist = t.asDinerosTransition();

            if (ist != null && ist.getMutableTransitionInformation() == null) {
                if (ist.getStaticTransitionInformation().isSignalTransitionInformation()) {
                    InputSignalClause isc = ist.getStaticTransitionInformation().asSignalTransitionInformation().getClause();
                    if (isc != null) {
                        System.out.println(isc.printClause());
                    }
                }
            }
        }

        if (withToolSpecifics) {
            System.out.println("--------------- TOOL SPECIFIC ---------------");

            for (Transition t : petriNet.allTransitions()) {
                DinerosTransition ist = t.asDinerosTransition();
                if (ist != null && ist.getNumToolspecific() > 0) {
                    System.out.println("ToolSpecific: (" + ist.getName().getText() + ") " + ist.getToolspecific(0).getFormattedXMLBuffer().toString());
                }
            }
        }
    }
}
