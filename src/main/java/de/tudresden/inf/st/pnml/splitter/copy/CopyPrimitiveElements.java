package de.tudresden.inf.st.pnml.splitter.copy;

import de.tudresden.inf.st.pnml.jastadd.model.*;

public class CopyPrimitiveElements {

    public static DinerosTransition copySignalTransition(DinerosTransition ist, String idPrefix){

        DinerosTransition istCopy = new DinerosTransition();
        istCopy.setId(idPrefix + ist.getId());
        istCopy.setMutableTransitionInformation(
                copySignalTransitionInformation(ist.getStaticTransitionInformation()
                        .asSignalTransitionInformation()));
        return istCopy;
    }

    public static TransitionInformation copySignalTransitionInformation (SignalTransitionInformation ti){

        SignalTransitionInformation tiCopy = new SignalTransitionInformation();

        tiCopy.setSubNet(ti.getSubNet());
        tiCopy.setNode(tiCopy.getNode());
        tiCopy.setTraceInfo(ti.getTraceInfo());

        return tiCopy;
    }

    private static InputSignalClause copyClause(InputSignalClause clause){

        InputSignalClause copy = new InputSignalClause();

        for(Disjunction d : clause.getDisjunctions()){
            Disjunction dCopy = new Disjunction();
            for(Literal l : d.getLiterals()){
                if(l.isNegativeLiteral()){
                    NegativeLiteral nl = new NegativeLiteral();
                    nl.setName(l.getName());
                    dCopy.addLiteral(nl);
                }else {
                    PositiveLiteral pl = new PositiveLiteral();
                    pl.setName(l.getName());
                    dCopy.addLiteral(pl);
                }
            }
            copy.addDisjunction(dCopy);
        }
        return copy;
    }
}
