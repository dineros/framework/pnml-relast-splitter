package de.tudresden.inf.st.pnml.splitter.constants;

public final class SplitterPnmlConstants {

    public static final String SPLITTER_TOOL_VERSION = "0.0.1";
    public static final String SPLITTER_TOOL_NAME = "de.tudresden.inf.st.pnml.splitter";

    public static final String TOOL_KEY = "tool";
    public static final String VERSION_KEY = "version";
    public static final String TOOLSPECIFIC_KEY = "toolspecific";
}