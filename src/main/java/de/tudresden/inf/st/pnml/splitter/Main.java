package de.tudresden.inf.st.pnml.splitter;

import de.tudresden.inf.st.pnml.jastadd.model.*;
import de.tudresden.inf.st.pnml.splitter.postprocessing.GlobalToLocalNetsPostProcessor;
import de.tudresden.inf.st.pnml.splitter.postprocessing.PostProcessingUtils;
import fr.lip6.move.pnml.framework.utils.exception.InvalidIDException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    // Utility to extract argument value by flag
    private static String getArgumentValue(String[] args, String flag, String defaultValue) {
        for (int i = 0; i < args.length; i++) {
            if (args[i].equals(flag) && i + 1 < args.length) {
                return args[i + 1];
            }
        }
        return defaultValue;
    }

    // Print help/usage instructions
    private static void printUsage() {
        System.out.println("Options:");
        System.out.println("  --pnml <path>                     Path to the PNML file (required)");
        System.out.println("  --help                            Show this help message and exit");
    }

    public static void main(String[] args) {

        if (args.length == 0 || Arrays.asList(args).contains("--help")) {
            printUsage();
            return;
        }

        // String configPath = getArgumentValue(args, "--config", null);
        String pnmlPath = getArgumentValue(args, "--pnml", null);

        // Validate required arguments
        if (pnmlPath == null) {
            System.err.println("Error: --pnml <path> is required.");
            printUsage();
            System.exit(1);
        }

        List<PetriNet> petriNets = PnmlParser.parsePnml(pnmlPath, true);
        JastAddList<ToolInfo> ti = petriNets.get(0).getToolspecificList().treeCopy();

        List<List<PetriNet>> disconnectedPetriNets = new ArrayList<>();
        GlobalToLocalNetsPostProcessor processor = new GlobalToLocalNetsPostProcessor();

        for (PetriNet pn : petriNets) {
            List<PetriNet> pnl = processor.disconnectNets(pn);

            for(PetriNet lrpn : pnl){
                lrpn.getToolspecificList().addAll(ti);
                lrpn.flushTreeCache();
            }

            disconnectedPetriNets.add(pnl);
        }

        for (List<PetriNet> petriNetList : disconnectedPetriNets) {
            for (PetriNet petriNet : petriNetList) {
                PostProcessingUtils.cleanEmptyPages(petriNet);
                petriNet.flushTreeCache();
            }
        }

        logger.info("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        for (int i = 0; i < disconnectedPetriNets.size(); i++) {
            for (int j = 0; j < disconnectedPetriNets.get(i).size(); j++) {
              //  logger.info("Exporting split Petri net containing: ");
               // PostProcessingUtils.printNet(disconnectedPetriNets.get(i).get(j), true, false);

                try {
                    PnmlExporter.serializeToPnmlFile(disconnectedPetriNets.get(i).get(j), "-pnml-net-" + i + "-" + j);
                } catch (InvalidIDException e) {
                    logger.error("An exception occurred while exporting the nets: " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }
}