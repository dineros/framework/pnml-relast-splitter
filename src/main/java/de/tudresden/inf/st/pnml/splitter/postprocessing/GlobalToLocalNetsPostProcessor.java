package de.tudresden.inf.st.pnml.splitter.postprocessing;

import de.tudresden.inf.st.pnml.base.constants.PnmlConstants;
import de.tudresden.inf.st.pnml.jastadd.model.*;
import de.tudresden.inf.st.pnml.splitter.constants.SplitterPnmlConstants;
import fr.lip6.move.pnml.framework.utils.exception.InvalidIDException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import java.io.StringWriter;
import java.util.*;

public class GlobalToLocalNetsPostProcessor implements PostProcessor<PetriNet> {

    private static final Logger logger = LoggerFactory.getLogger(GlobalToLocalNetsPostProcessor.class);

    private final Map<String, Map<DinerosPlace, Integer>> topicSubscriberPlaces = new HashMap<>();
    private final Map<String, Map<DinerosPlace, Integer>> topicPublisherPlaces = new HashMap<>();
    private final Map<String, List<Tuple2<DinerosPlace, String>>> serviceClientResPlaces = new HashMap<>();
    private final Map<String, List<Tuple2<DinerosPlace, String>>> serviceClientReqPlaces = new HashMap<>();
    private final Map<String, Tuple2<DinerosPlace, Integer>> serviceServerResPlaces = new HashMap<>();
    private final Map<String, Tuple2<DinerosPlace, Integer>> serviceServerReqPlaces = new HashMap<>();

    public List<PetriNet> disconnectNets(PetriNet petriNet) {

        try {
            // init list of distributed nets
            List<PetriNet> pns = new ArrayList<>();

            // cut the topic transitions
            PetriNet topicCutNet = cutTopicTransitions(petriNet);

            // cut the service transitions
            PetriNet serviceCutNet = cutServiceTransitions(topicCutNet);

            Set<String> locations = getNodePageIds(serviceCutNet);

            updateToolSpecificsWithPlaceInfos(serviceCutNet);

            //logger.info("##############################################################################################");

            // serialize for deep copy
            String serializedNetPath = PnmlExporter.serializeToPnmlFile(serviceCutNet, "-pre-split");

            int netSuffix = 0;

            // filter deep copy elements by location
            for (String location : locations) {

                System.out.println("Creating deep copy for: " + location);
                List<PetriNet> reParsedPetriNets = PnmlParser.parsePnml(serializedNetPath, true);

                for (PetriNet pn : reParsedPetriNets) {
                    PetriNet separatedNet = createdSeparatedNetByNode(pn, location);
                    separatedNet.setId(separatedNet.getType() + "-" + netSuffix);
                    pns.add(separatedNet);
                }
            }

            // return the filtered distributed nets
            return pns;
        } catch (InvalidIDException e) {
            logger.error("Could not handle: " + e.getMessage());
            return null;
        }
    }

    private static Set<String> getNodePageIds(PetriNet petriNet){

        Set<String> nodePagesIds = new HashSet<>();

        for(Page page : petriNet.allPages()){
            if(page.getType() != null && page.getType().equals(PnmlConstants.PAGE_TYPE_NODE)){
                System.out.println("Found node page: " + page.getId());
                nodePagesIds.add(page.getId());
            }
        }

        return nodePagesIds;
    }

    private PetriNet cutServiceTransitions(PetriNet petriNet) {

        Set<Transition> transitionsToRemove = new HashSet<>();

        for (Transition transition : petriNet.allTransitions()) {

            DinerosTransition serviceTransition = transition.asDinerosTransition();

            if (serviceTransition.getStaticTransitionInformation().isServiceTransitionInformation()) {
                logger.info("Processing service transition: " + transition.getId());

                String serviceName = serviceTransition.getStaticTransitionInformation()
                        .asServiceTransitionInformation().getServiceName();

                // Add to the deprecated transitions
                transitionsToRemove.add(transition);

                for (ServiceChannel sc : serviceTransition.getStaticTransitionInformation().
                        asServiceTransitionInformation().getClientChannelList()) {

                    if (!serviceClientResPlaces.containsKey(serviceName)) {
                        serviceClientResPlaces.put(serviceName, new ArrayList<>());
                    }
                    if (!serviceClientReqPlaces.containsKey(serviceName)) {
                        serviceClientReqPlaces.put(serviceName, new ArrayList<>());
                    }
                    serviceClientReqPlaces.get(serviceName).add(new Tuple2<>(petriNet.getPlaceById(sc.getRequestPlaceId()).asDinerosPlace(), sc.getResponsePlaceId()));
                    serviceClientResPlaces.get(serviceName).add(new Tuple2<>(petriNet.getPlaceById(sc.getResponsePlaceId()).asDinerosPlace(), sc.getRequestPlaceId()));
                }

                serviceServerReqPlaces.put(serviceName, new Tuple2<>(petriNet.getPlaceById(serviceTransition.
                        getStaticTransitionInformation().asServiceTransitionInformation().getServerChannel().getRequestPlaceId()).asDinerosPlace(),
                        serviceTransition.getStaticTransitionInformation().asServiceTransitionInformation().getServerChannel().getCapacity()));
                serviceServerResPlaces.put(serviceName, new Tuple2<>(petriNet.getPlaceById(serviceTransition.
                        getStaticTransitionInformation().asServiceTransitionInformation().getServerChannel().getResponsePlaceId()).asDinerosPlace(),
                        serviceTransition.getStaticTransitionInformation().asServiceTransitionInformation().getServerChannel().getCapacity()));
            }
        }

        // Remove "old" transitions
        for (Transition t : transitionsToRemove) {
            t.removeSelf();
        }

        petriNet.flushTreeCache();
        return petriNet;
    }

    private PetriNet cutTopicTransitions(PetriNet petriNet) {

        Set<Transition> transitionsToRemove = new HashSet<>();

        for (Transition transition : petriNet.allTransitions()) {

            DinerosTransition topicTransition = transition.asDinerosTransition();

            if (topicTransition.getStaticTransitionInformation().isTopicTransitionInformation()) {
                logger.info("Processing topic transition: " + transition.getId());

                String topic = topicTransition.getStaticTransitionInformation().asTopicTransitionInformation().getTopic();

                // Add to the deprecated transitions
                transitionsToRemove.add(transition);

                for (SubscriberPort subscriberPort : topicTransition.getStaticTransitionInformation()
                        .asTopicTransitionInformation().getSubscriberPorts()) {

                    Place outPlace = petriNet.getPlaceById(subscriberPort.getPlaceId());
                    if (!topicSubscriberPlaces.containsKey(topic)) {
                        topicSubscriberPlaces.put(topic, new HashMap<>());
                    }

                    topicSubscriberPlaces.get(topic).put(outPlace.asDinerosPlace(), subscriberPort.getLimit());
                }

                for (PublisherPort publisherPort : topicTransition.getStaticTransitionInformation()
                        .asTopicTransitionInformation().getPublisherPorts()) {

                    Place inPlace = petriNet.getPlaceById(publisherPort.getPlaceId());
                    if (!topicPublisherPlaces.containsKey(topic)) {
                        topicPublisherPlaces.put(topic, new HashMap<>());
                    }

                   topicPublisherPlaces.get(topic).put(inPlace.asDinerosPlace(), publisherPort.getLimit());
                }
            }
        }

        // Remove "old transition"
        for (Transition t : transitionsToRemove) {
            t.removeSelf();
        }

        petriNet.flushTreeCache();

        return petriNet;
    }

    private static PetriNet createdSeparatedNetByNode(PetriNet petriNet, String node) {

        System.out.println("Creating separate net for: " + node);

        Set<String> removedTransitionIds = new HashSet<>();
        Set<String> removedPlaceIds = new HashSet<>();
        Set<String> removedRefTransitionIds = new HashSet<>();
        Set<String> removeRefPlaceIds = new HashSet<>();

        deleteOnNoMatch(node, petriNet.allPlaces(), removedPlaceIds);
        deleteOnNoMatch(node, petriNet.allTransitions(), removedTransitionIds);
        deleteOnNoMatch(node, petriNet.allRefPlaces(), removeRefPlaceIds);
        deleteOnNoMatch(node, petriNet.allRefTransitions(), removedRefTransitionIds);

        for (Arc arc : petriNet.allArcs()) {
            if (removedTransitionIds.contains(arc.getSource().getId()) || removedTransitionIds.contains(arc.getTarget().getId())
                    || removedPlaceIds.contains(arc.getSource().getId()) || removedPlaceIds.contains(arc.getTarget().getId())
                    || removedRefTransitionIds.contains(arc.getSource().getId()) || removedRefTransitionIds.contains(arc.getTarget().getId())
                    || removeRefPlaceIds.contains(arc.getSource().getId()) || removeRefPlaceIds.contains(arc.getTarget().getId())) {
                arc.removeSelf();
            }
        }

        for (Place place : petriNet.allPlaces()) {
            if (removedPlaceIds.contains(place.getId())) {
                System.out.println("removing: " + place.getId());
                place.removeSelf();
            }
        }

        for (RefPlace place : petriNet.allRefPlaces()) {
            if (removeRefPlaceIds.contains(place.getId())) {
                System.out.println("removing: " + place.getId());
                place.removeSelf();
            }
        }

        for (RefTransition transition : petriNet.allRefTransitions()) {
            if (removedRefTransitionIds.contains(transition.getId())) {
                transition.removeSelf();
            }
        }

        for (Transition transition : petriNet.allTransitions()) {

            if (removedTransitionIds.contains(transition.getId())) {
                transition.removeSelf();
            }

            if (transition.asDinerosTransition().getStaticTransitionInformation().isTopicTransitionInformation()) {
                if (!transition.asDinerosTransition().getStaticTransitionInformation().asTopicTransitionInformation().getNode().equals(node)) {
                    logger.info("REMOVING OLD TOPIC TRANSITION " + transition.getId() + " from net");
                    transition.removeSelf();
                }
            }

            if (transition.asDinerosTransition().getStaticTransitionInformation().isServiceTransitionInformation()) {
                if (!transition.asDinerosTransition().getStaticTransitionInformation().asServiceTransitionInformation().getNode().equals(node)) {
                    logger.info("REMOVING OLD SERVICE TRANSITION " + transition.getId() + " from net");
                    transition.removeSelf();
                }
            }
        }

        petriNet.flushTreeCache();
        petriNet.flushAttrAndCollectionCache();

        return petriNet;
    }

    private static void deleteOnNoMatch(String node, Set<? extends PnObject> searchable, Set<String> removedObjects) {
        for (PnObject o : searchable) {
            if(!o.isArcNode()) {
                boolean matches = false;
                for (Page cPage : o.ContainingPageTree()) {
                    if (cPage.getId().equals(node)) {
                        matches = true;
                        break;
                    }
                }
                if (!matches) {
                    System.out.println("Removing Object: " + o.getId());
                    removedObjects.add(o.getId());
                }
            }
        }
    }

    private void updateToolSpecificsWithPlaceInfos(PetriNet petriNet) {

        ToolInfo ti = buildToolSpecifics();
        JastAddList<ToolInfo> tiList = new JastAddList<>();
        tiList.add(ti);
        petriNet.setToolspecificList(tiList);
        petriNet.flushTreeCache();

    }

    private void buildPlaceInfoListPartWithoutCap(Document doc, Element ports, Map<String, List<Tuple2<DinerosPlace, String>>> map, String placeType) {

        Map<String, Map<DinerosPlace, Integer>> defaultMap = new HashMap<>();

        for(String s : map.keySet()){

            Map<DinerosPlace, Integer> tMap = new HashMap<>();
            for(Tuple2<DinerosPlace, String> t : map.get(s)){
                tMap.put(t.get_1(), -1);
            }
            defaultMap.put(s, tMap);
        }

        buildPlaceInfoListPart(doc, ports, defaultMap, placeType, false, true);

    }

    private void buildPlaceInfoListPart(Document doc, Element ports, Map<String, Map<DinerosPlace, Integer>> map, String placeType, boolean withLimit, boolean isClient) {

        for (Map.Entry<String, Map<DinerosPlace, Integer>> entry : map.entrySet()) {
            for (Map.Entry<DinerosPlace, Integer> innerEntry : entry.getValue().entrySet()) {

                Element port = doc.createElement(PnmlConstants.CHANNEL_PORT_KEY);
                port.appendChild(doc.createTextNode(innerEntry.getKey().getId()));

                Attr attr1 = doc.createAttribute(PnmlConstants.CHANNEL_NAME_KEY);
                attr1.setValue(entry.getKey());
                port.setAttributeNode(attr1);

                Attr attr2 = doc.createAttribute(PnmlConstants.CHANNEL_PLACE_TYPE_KEY);
                attr2.setValue(placeType);
                port.setAttributeNode(attr2);

                if(withLimit) {
                    Attr attr3 = doc.createAttribute(PnmlConstants.CHANNEL_LIMIT_KEY);
                    attr3.setValue(String.valueOf(innerEntry.getValue()));
                    port.setAttributeNode(attr3);
                }

                if(placeType.equals(PnmlConstants.CHANNEL_PLACE_TYPE_CLIENT_REQ_KEY)){
                    Attr attr4 = doc.createAttribute(PnmlConstants.PORTS_CRES_KEY);
                    findOppositePlace(innerEntry, port, attr4, serviceClientReqPlaces);
                }

                if(placeType.equals(PnmlConstants.CHANNEL_PLACE_TYPE_CLIENT_RES_KEY)){
                    Attr attr4 = doc.createAttribute(PnmlConstants.PORTS_CREQ_KEY);
                    findOppositePlace(innerEntry, port, attr4, serviceClientResPlaces);
                }

                ports.appendChild(port);
            }
        }
    }

    private void findOppositePlace(Map.Entry<DinerosPlace, Integer> innerEntry, Element port, Attr attr4, Map<String, List<Tuple2<DinerosPlace, String>>> map) {
        for(Map.Entry<String, List<Tuple2<DinerosPlace, String>>> e : map.entrySet()){
            for(Tuple2<DinerosPlace, String> et : e.getValue()){
                if(et.get_1().getId().equals(innerEntry.getKey().getId())){
                    attr4.setValue(et.get_2());
                    port.setAttributeNode(attr4);
                }
            }
        }
    }

    private void buildPlaceInfoListServerPart(Document doc, Element ports, Map<String, Tuple2<DinerosPlace, Integer>> map, String placeType) {

        for (Map.Entry<String, Tuple2<DinerosPlace, Integer>> entry : map.entrySet()) {

                Element port = doc.createElement(PnmlConstants.CHANNEL_PORT_KEY);
                port.appendChild(doc.createTextNode(entry.getValue().get_1().getId()));

                Attr attr1 = doc.createAttribute(PnmlConstants.CHANNEL_NAME_KEY);
                attr1.setValue(entry.getKey());
                port.setAttributeNode(attr1);

                Attr attr2 = doc.createAttribute(PnmlConstants.CHANNEL_PLACE_TYPE_KEY);
                attr2.setValue(placeType);
                port.setAttributeNode(attr2);

                Attr attr3 = doc.createAttribute(PnmlConstants.CHANNEL_LIMIT_KEY);
                attr3.setValue(String.valueOf(entry.getValue().get_2()));
                port.setAttributeNode(attr3);

                ports.appendChild(port);
        }
    }

    private ToolInfo buildToolSpecifics() {

        // build place infos
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement(SplitterPnmlConstants.TOOLSPECIFIC_KEY);
            rootElement.setAttribute(SplitterPnmlConstants.TOOL_KEY, SplitterPnmlConstants.SPLITTER_TOOL_NAME);
            rootElement.setAttribute(SplitterPnmlConstants.VERSION_KEY, SplitterPnmlConstants.SPLITTER_TOOL_VERSION);
            doc.appendChild(rootElement);

            Element ports = doc.createElement(PnmlConstants.CHANNEL_PORTS_KEY);

            buildPlaceInfoListPart(doc, ports, topicPublisherPlaces, PnmlConstants.CHANNEL_PLACE_TYPE_PUB_KEY, true, false);
            buildPlaceInfoListPart(doc, ports, topicSubscriberPlaces, PnmlConstants.CHANNEL_PLACE_TYPE_SUB_KEY, true, false);
            buildPlaceInfoListPartWithoutCap(doc, ports, serviceClientReqPlaces, PnmlConstants.CHANNEL_PLACE_TYPE_CLIENT_REQ_KEY);
            buildPlaceInfoListPartWithoutCap(doc, ports, serviceClientResPlaces, PnmlConstants.CHANNEL_PLACE_TYPE_CLIENT_RES_KEY);
            buildPlaceInfoListServerPart(doc, ports, serviceServerReqPlaces, PnmlConstants.CHANNEL_PLACE_TYPE_SERVER_REQ_KEY);
            buildPlaceInfoListServerPart(doc, ports, serviceServerResPlaces, PnmlConstants.CHANNEL_PLACE_TYPE_SERVER_RES_KEY);

            rootElement.appendChild(ports);

            TransformerFactory tf = TransformerFactory.newInstance();
            Transformer trans = tf.newTransformer();
            StringWriter sw = new StringWriter();
            trans.transform(new DOMSource(doc), new StreamResult(sw));

            ToolInfo toolInfo = new ToolInfo();
            toolInfo.setFormattedXMLBuffer(sw.getBuffer());
            toolInfo.setVersion(SplitterPnmlConstants.SPLITTER_TOOL_VERSION);
            toolInfo.setTool(SplitterPnmlConstants.SPLITTER_TOOL_NAME);

            return toolInfo;
        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException e) {
            throw new RuntimeException(e);
        }

        return null;
    }
}